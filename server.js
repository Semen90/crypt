var express = require('express');
var path = require('path');
var request = require('request');
var router = express.Router();  
var bodyParser = require('body-parser');
var fallback = require('express-history-api-fallback');
var os = require('os');
var fs = require('fs');
var arraySort = require('array-sort');

var app = express();

var root = __dirname + '/dist';
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(router);
app.use(express.static(path.join(root)));
app.use(fallback('index.html', { root: root }));
app.set('port', process.env.PORT || 5000);



router.get('/', function(req, res){
    res.sendFile(path.join(root + '/index.html'));
});


function getCurrencies(currenciesData) {

    var currencies = JSON.parse(currenciesData);
    var array = [];
    
    for(key in currencies) {
        array.push({
            key: key,
            data: currencies[key]                
        })
    }
    arraySort(array, 'key');
    return array;
}

function sendCurrenciesData(socket) {
    setInterval(function(){
        request('https://poloniex.com/public?command=returnTicker', function (error, response, body) {
            if (error) {
                console.log('error:', error);
                return;                
            }
    
            var currencies = getCurrencies(response.body);   
            socket.emit('currenciesReady', currencies);             
        });
    }, 8000); 
}

function sendOneCurrenciesData(socket) {
    request('https://poloniex.com/public?command=returnTicker', function (error, response, body) {
        if (error) {
            console.log('error:', error);
            return;
        }            
        
        var currencies = getCurrencies(response.body);        
        socket.emit('currenciesFirstConnect', currencies);
    });
}


var server = require('http').Server(app);
var io = require('socket.io')(server);

server.listen(app.get('port'), function(){
    console.log('Server start http://localhost:' + app.get('port'));
    console.log(' ');  
});

io.on('connection', function (socket) {
    sendOneCurrenciesData(socket);
    sendCurrenciesData(socket);
});