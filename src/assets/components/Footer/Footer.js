import React from 'react';
import './footer.scss';

class Footer extends React.Component {
    render() {
        return(
            <footer className="footer">
                <div className="inner">Footer</div>                
            </footer>
        );
    }
}

export default Footer;