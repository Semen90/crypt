import {combineReducers} from 'redux';
import { actionsMain } from './actions';
import { routerReducer } from 'react-router-redux'

const initialState = {
    value: 1
}

const mainReducer = (state = initialState, action) => {
    switch(action.type) {
        case actionsMain.MY_ACTION:
            return {...state, value: 2}
        default:
            return state;
    }
}

const reducer = combineReducers({
    main: mainReducer,
    router: routerReducer
});

export default reducer;