import React from 'react';
import './currency.css';

class Currency extends React.Component {
    constructor(props) {
        super(props);

        this.renderAll = this.renderAll.bind(this);
    }

    renderAll() {
        const items = [];
        const quotes = this.props.currData.list;
        quotes.forEach((item, i)=> {
            items.push( <div className="currency-quote__item" key={i}>{item.currencies[1]} = {item.data.last} {item.currencies[0]} </div> )
        });
        return items;
    }


    render() {
        return(
            <div className="currency">
                <div className="currency__name">{this.props.currData.currency}</div>                
                <div className="currency-quote">
                    {this.renderAll()}
                </div>
            </div>
        );
    }
}

export default Currency;