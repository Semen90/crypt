import React from 'react';
import { Link } from 'react-router-dom'

import './header.scss';

class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <div className="inner">Header</div>                
            </header>
        );        
    }
}

export default Header;