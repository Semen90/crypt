export const actionsMain = {
    MY_ACTION: 'MY_ACTION'
} 

export const startAction = () => {
    return { type: actionsMain.MY_ACTION }
}