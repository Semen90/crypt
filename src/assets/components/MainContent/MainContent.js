import React from 'react';
import CurrenciesBox from '../CurrenciesBox/CurrenciesBox';

class MainContent extends React.Component {
    constructor() {
        super();
    }

    render() {
        return(
            <div className="main-content">
                <div className="inner">
                    <CurrenciesBox />
                </div>
            </div>
        )
    }
}

export default MainContent;
