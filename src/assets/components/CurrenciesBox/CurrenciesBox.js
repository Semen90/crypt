import React from 'react';
import io from 'socket.io-client';

import './currenciesBox.css';
import Currency from '../Currency/Currency';


class CurrenciesBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currencies: []
        }
        this.startSocket = this.startSocket.bind(this);
        this.renderCurrencies = this.renderCurrencies.bind(this);
        this.generateCurrenciesArray = this.generateCurrenciesArray.bind(this);

    }    

    componentDidMount() {
        this.startSocket();
    }

    startSocket() {    
        const socket = io();      

        socket.on('currenciesFirstConnect', (data) => {
            this.setState({currencies: data});
        });

        socket.on('currenciesReady', (data) => {
            this.setState({currencies: data});
        });
    }

    renderCurrencies() {
        const currencies = this.state.currencies;
        let render = [];
        let currencyItems = [];

        for(var i = 0; i < currencies.length; i++) {
            var split = currencies[i].key.split('_');
            render.push( {currencies:split, data: currencies[i].data} );
        }
        render = this.generateCurrenciesArray(render);    
        
        // for render
        for(var i = 0; i < render.length; i++) {
            currencyItems.push( <Currency currData={render[i]} key={i}/> );
        }

        return currencyItems;
    }

    generateCurrenciesArray(arr) {
        const newArr = [];
        var current = '';

        for(var i = 0; i < arr.length; i++) {
            const newCurrent = arr[i].currencies[0];
            
            if(newCurrent !== current) { //пришла новая валюта                
                current = arr[i].currencies[0];

                // если встрелось совпадение в неотсортированном массиве
                let hasCoincidence = false;
                for(var j = 0; j < newArr.length; j++) {
                    if (current === newArr[j].currency) {
                        newArr[j].list.push(arr[i])
                        hasCoincidence = true;
                    }
                }
                // END

                if(!hasCoincidence) {
                    let obj = {
                        currency: current,
                        list: []
                    }
        
                    for(var j = 0; j < arr.length; j++) {
                        if( current === arr[j].currencies[0]) {
                            obj.list.push(arr[j]);
                        }
                    }        
                    newArr.push(obj);
                }
            }
        }
        return newArr;
    }
    
    render() {
        if(this.state.currencies.length > 0) {
            return(
                <div className="currencies-box">
                    {this.renderCurrencies()}
                </div>
            );
        } else {
            return(
                <div className="currencies-box">
                    Загрузка данных...
                </div>
            );
        }
        
    }
}

export default CurrenciesBox;